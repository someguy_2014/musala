ALTER TABLE
  devices
ADD
  CONSTRAINT fk_gateway FOREIGN KEY (gateway_id) REFERENCES gateways(id);