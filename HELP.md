# Musala Gateways & Devices

### Implemented features

* Gateways crud functionality in OneToMany relation
* Devices crud functionality in ManyToOne relation
* MySql Docker container - 2x databses `gateways` and `gateways_test`
* Flyway database migrations decoupled from the application and the tests
* Automation tests and `database cleaner` - create/drop the tables before/after are executed. Before and after each test execution, the tables are truncated.

### Will be continued

* Basic security for the backend
* React app that interacts with the backend
* OAuth security for the React app

### Database configuration

*run MySql on docker container*
```
docker-machine start default
eval "$(docker-machine env default)"
docker-compose up --force-recreate --build
```
*Config files*
```
Database migration config files
flyway/application.properties
flyway/test.properties

Application config files
config/application.properties
config/test.properties
```
*Execute database migrations*
In the `flyway` folder run
```
mvn clean flyway:migrate -Dflyway.configFiles=application.properties
mvn clean flyway:migrate -Dflyway.configFiles=test.properties
```

### Start the application

In the project folder
```
mvn test
```
```
mvn spring-boot:run
```

### Curl request

* Get all gateways

```
curl http://localhost:8080/gateway/
```

* Create gateway
```
curl -X POST -H "Content-Type:application/json" -d '{ "uniqueSerialNumber": "42a02e7e-2ac5-4da8-8edf-a37a363fc0fd", "name": "gateway 1", "ip": "192.168.1.10" }' http://localhost:8080/gateway/
```

* Update gateway
```
curl -X PUT -H "Content-Type:application/json" -d '{ "uniqueSerialNumber": "42a02e7e-2ac5-4da8-8edf-a37a363fc0fe", "name": "gateway 1.1", "ip": "192.168.1.10" }' http://localhost:8080/gateway/1
```

* Get all gateways
```
curl http://localhost:8080/gateway
```

* Get gateway
```
curl http://localhost:8080/gateway/1
```
* Delete gateway
```
curl -X DELETE http://localhost:8080/gateway/1
```

* Create device
```
curl -X POST -H "Content-Type:application/json" -d '{ "vendor": "Vendor 1", "status": "online", "gateway_id": "1" }' http://localhost:8080/gateway/1/device
```

* Update device
```
curl -X PUT -H "Content-Type:application/json" -d '{ "vendor": "Vendor 1", "status": "offline", "gateway_id": "1" }' http://localhost:8080/gateway/1/device/1
```

* Get devices
```
curl http://localhost:8080/gateway/1/device
```

* Get device
```
curl http://localhost:8080/gateway/1/device/1
```

* Delete device
```
curl -X DELETE http://localhost:8080/gateway/1/device/1
```

