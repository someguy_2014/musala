package com.musala.gateways.service;

import static java.util.stream.Collectors.toList;
import java.util.List;
import java.util.stream.StreamSupport;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.repository.GatewayRepository;
import java.text.MessageFormat;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class GatewayService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE = "Could not find gateway with id: {0}";
    public static final String NOT_SUPPORTED_FILED = "Field name: '{0}' is not supported";
    public static final String SERIAL_NUMBER_ALREADY_EXISTS = "uniqueSerialNumber must be unique";

    private final GatewayRepository gatewayRepository;

    @Autowired
    public GatewayService(GatewayRepository gatewayRepository) {
        this.gatewayRepository = gatewayRepository;
    }

    public Gateway addGateway(Gateway gateway) {
        validateUniqueSerialNumber(gateway);

        return gatewayRepository.save(gateway);
    }

    public List<Gateway> getGateways() {
        return StreamSupport.stream(gatewayRepository.findAll().spliterator(), false)
                .collect(toList());
    }

    public Gateway getGateway(Integer id) {
        return gatewayRepository.findById(id).orElseThrow(() -> createResourceNotFoundException(id));
    }

    public Gateway deleteGateway(Integer id) {
        Gateway gateway = getGateway(id);

        gatewayRepository.delete(gateway);

        return gateway;
    }

    public Gateway editGateway(Integer id, Gateway gateway) {
        Gateway gatewayToEdit = getGateway(id);
        gatewayToEdit.setUniqueSerialNumber(gateway.getUniqueSerialNumber());
        gatewayToEdit.setName(gateway.getName());
        gatewayToEdit.setIp(gateway.getIp());

        validateUniqueSerialNumber(gatewayToEdit);

        return gatewayRepository.save(gatewayToEdit);
    }

    public void validateUniqueSerialNumber(Gateway gateway) {
        boolean isValid = isValid(gateway.getId(), gateway.getUniqueSerialNumber(), "uniqueSerialNumber");

        if (isValid) {
            return;
        }

        throw new ValidationException(SERIAL_NUMBER_ALREADY_EXISTS);
    }

    private boolean isValid(Integer gatewayId, Object value, String fieldName) throws UnsupportedOperationException {
        if (fieldName.equals("uniqueSerialNumber")) {
            Gateway found = gatewayRepository.findByUniqueSerialNumber(String.valueOf(value));
            if (found == null) {
                return true;
            }

            return found.getId() == gatewayId;
        }

        throw new UnsupportedOperationException(MessageFormat.format(NOT_SUPPORTED_FILED, fieldName));
    }

    private ResourceNotFoundException createResourceNotFoundException(Integer id) {
        return new ResourceNotFoundException(MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id));
    }

}
