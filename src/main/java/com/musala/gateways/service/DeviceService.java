package com.musala.gateways.service;

import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.repository.DeviceRepository;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import javax.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {

    public static final String RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE = "Could not find device with id: {0} for gateway id: {1}";
    public static final String DEVICES_MAX_SIZE_ERROR_MESSAGE_TEMPLATE = "Gateway devices max size of {0} exceeded";
    public static final Integer GATEWAY_DEVICES_MAX_SIZE = 10;

    private final DeviceRepository deviceRepository;
    private final ReentrantLock lock = new ReentrantLock();

    @Autowired
    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public Device addDevice(Gateway gateway, Device device) {
        try {
            lock.lock();
            if (getGatewayDevicesCount(gateway) >= GATEWAY_DEVICES_MAX_SIZE) {
                throw new ValidationException(MessageFormat.format(DEVICES_MAX_SIZE_ERROR_MESSAGE_TEMPLATE, GATEWAY_DEVICES_MAX_SIZE));
            }

            device.setGateway(gateway);
            return deviceRepository.save(device);
        } finally {
            lock.unlock();
        }
    }

    public List<Device> getDevices(Integer gatewayId) {
        return deviceRepository.findByGatewayId(gatewayId);
    }

    public Device getDevice(Integer gatewayId, Integer id) {
        return deviceRepository.findByIdAndGatewayId(id, gatewayId).orElseThrow(() -> createResourceNotFoundException(gatewayId, id));
    }

    public Device deleteDevice(Integer gatewayId, Integer id) {
        Device device = getDevice(gatewayId, id);

        deviceRepository.delete(device);

        return device;
    }

    public Device editDevice(Integer gatewayId, Integer id, Device device) {
        Device deviceToEdit = getDevice(gatewayId, id);
        deviceToEdit.setVendor(device.getVendor());
        deviceToEdit.setStatus(device.getStatus());

        return deviceRepository.save(deviceToEdit);
    }

    public Integer getGatewayDevicesCount(Gateway gateway) {
        return deviceRepository.getGatewayDevicesCount(gateway);
    }

    private ResourceNotFoundException createResourceNotFoundException(Integer gatewayId, Integer id) {
        return new ResourceNotFoundException(MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, id, gatewayId));
    }

}
