package com.musala.gateways.config;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.FileInputStream;
import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Value("${properties_path}")
    private String properties_path;

    @Bean
    public DataSource dataSource() throws Throwable {
        Properties p = new Properties();
        p.load(new FileInputStream(properties_path));

        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(p.getProperty("spring.datasource.url"));
        dataSource.setUser(p.getProperty("spring.datasource.username"));
        dataSource.setPassword(p.getProperty("spring.datasource.password"));

        return dataSource;
    }
}
