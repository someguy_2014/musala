package com.musala.gateways.controller;

import static java.util.stream.Collectors.toList;
import java.util.List;
import javax.validation.Valid;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.dto.GatewayDto;
import com.musala.gateways.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gateway")
public class GatewayController {

    private final GatewayService gatewayService;

    @Autowired
    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @PostMapping
    public ResponseEntity<GatewayDto> addGateway(@RequestBody @Valid GatewayDto gatewayDto) {
        Gateway gateway = gatewayService.addGateway(gatewayDto.toGateway());
        return new ResponseEntity<>(GatewayDto.from(gateway), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<GatewayDto>> getGateways() {
        List<GatewayDto> gatewaysDto = gatewayService.getGateways().stream().map(GatewayDto::from).collect(toList());
        return new ResponseEntity<>(gatewaysDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<GatewayDto> getGateway(@PathVariable Integer id) {
        Gateway gateway = gatewayService.getGateway(id);
        return new ResponseEntity<>(GatewayDto.from(gateway), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<GatewayDto> editGateway(@PathVariable final Integer id,
            @RequestBody GatewayDto gatewayDto) {
        Gateway gateway = gatewayService.editGateway(id, gatewayDto.toGateway());
        return new ResponseEntity<>(GatewayDto.from(gateway), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<GatewayDto> deleteGateway(@PathVariable Integer id) {
        Gateway gateway = gatewayService.deleteGateway(id);
        return new ResponseEntity<>(GatewayDto.from(gateway), HttpStatus.OK);
    }
}
