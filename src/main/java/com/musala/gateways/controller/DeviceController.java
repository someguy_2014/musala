package com.musala.gateways.controller;

import java.util.List;
import javax.validation.Valid;
import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.dto.DeviceDto;
import com.musala.gateways.service.DeviceService;
import com.musala.gateways.service.GatewayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/gateway/{gatewayId}/device")
public class DeviceController {

    private final GatewayService gatewayService;
    private final DeviceService deviceService;

    @Autowired
    public DeviceController(GatewayService gatewayService, DeviceService deviceService) {
        this.gatewayService = gatewayService;
        this.deviceService = deviceService;
    }

    @PostMapping
    public ResponseEntity<DeviceDto> addDevice(@PathVariable("gatewayId") Integer gatewayId,
            @RequestBody @Valid DeviceDto deviceDto) {
        Gateway gateway = gatewayService.getGateway(gatewayId);
        Device device = deviceService.addDevice(gateway, deviceDto.toDevice());

        return new ResponseEntity<>(DeviceDto.from(device), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<DeviceDto>> getDevices(
            @PathVariable("gatewayId") Integer gatewayId) {
        List<DeviceDto> devicesDto
                = deviceService.getDevices(gatewayId).stream().map(DeviceDto::from).collect(toList());

        return new ResponseEntity<>(devicesDto, HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<DeviceDto> getDevive(@PathVariable("gatewayId") Integer gatewayId,
            @PathVariable("id") Integer id) {
        Device device = deviceService.getDevice(gatewayId, id);
        return new ResponseEntity<>(DeviceDto.from(device), HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<DeviceDto> editDevice(
            @PathVariable(value = "gatewayId") Integer gatewayId,
            @PathVariable(value = "id") Integer id, @RequestBody @Valid DeviceDto deviceDto) {
        Device device = deviceService.editDevice(gatewayId, id, deviceDto.toDevice());
        return new ResponseEntity<>(DeviceDto.from(device), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<DeviceDto> deleteDevice(@PathVariable(value = "id") Integer id,
            @PathVariable(value = "gatewayId") Integer gatewayId) {
        Device device = deviceService.deleteDevice(gatewayId, id);
        return new ResponseEntity<>(DeviceDto.from(device), HttpStatus.OK);
    }
}
