package com.musala.gateways.model.dto;

import com.musala.gateways.model.Device;
import com.musala.gateways.model.enums.DeviceStatus;
import java.time.LocalDateTime;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class DeviceDto {

    private Integer id;
    private String vendor;
    @Enumerated(EnumType.STRING)
    private DeviceStatus status;
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public DeviceStatus getStatus() {
        return status;
    }

    public void setStatus(DeviceStatus status) {
        this.status = status;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Device toDevice() {
        Device device = new Device();
        device.setVendor(getVendor());
        device.setStatus(getStatus());
        return device;
    }

    public static DeviceDto from(Device device) {
        DeviceDto deviceDto = new DeviceDto();
        deviceDto.setId(device.getId());
        deviceDto.setVendor(device.getVendor());
        deviceDto.setStatus(device.getStatus());
        deviceDto.setDateCreated(device.getDateCreated());
        deviceDto.setDateUpdated(device.getDateUpdated());
        return deviceDto;
    }

}
