package com.musala.gateways.model.dto;

import java.util.HashSet;
import java.util.Set;
import com.musala.gateways.model.Gateway;
import java.time.LocalDateTime;
import static java.util.stream.Collectors.toSet;
import javax.validation.constraints.Pattern;

public class GatewayDto {

    private static final String IPV4_REGEX_STRING
            = "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.(?!$)|$)){4}$";

    private Integer id;

    private String uniqueSerialNumber;

    private String name;

    @Pattern(regexp = IPV4_REGEX_STRING, message = "Enter valid IPv4 IP")
    private String ip;

    private Set<DeviceDto> devices = new HashSet<>();

    private LocalDateTime dateCreated;

    private LocalDateTime dateUpdated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniqueSerialNumber() {
        return uniqueSerialNumber;
    }

    public void setUniqueSerialNumber(String uniqueSerialNumber) {
        this.uniqueSerialNumber = uniqueSerialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Set<DeviceDto> getDevices() {
        return devices;
    }

    public void setDevices(Set<DeviceDto> devices) {
        this.devices = devices;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Gateway toGateway() {
        Gateway gateway = new Gateway();
        gateway.setUniqueSerialNumber(getUniqueSerialNumber());
        gateway.setName(getName());
        gateway.setIp(getIp());
        return gateway;
    }

    public static GatewayDto from(Gateway gateway) {
        GatewayDto gatewayDto = new GatewayDto();
        gatewayDto.setId(gateway.getId());
        gatewayDto.setUniqueSerialNumber(gateway.getUniqueSerialNumber());
        gatewayDto.setName(gateway.getName());
        gatewayDto.setIp(gateway.getIp());
        gatewayDto.setDevices(gateway.getDevices().stream().map(device -> DeviceDto.from(device)).collect(toSet()));
        gatewayDto.setDateCreated(gateway.getDateCreated());
        gatewayDto.setDateUpdated(gateway.getDateUpdated());
        return gatewayDto;
    }

}
