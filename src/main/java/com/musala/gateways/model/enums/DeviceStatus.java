package com.musala.gateways.model.enums;

public enum DeviceStatus {
    online, offline;
}
