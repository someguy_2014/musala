package com.musala.gateways.repository;

import java.util.List;
import java.util.Optional;
import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface DeviceRepository extends CrudRepository<Device, Integer> {

    List<Device> findByGatewayId(Integer gatewayId);

    Optional<Device> findByIdAndGatewayId(Integer id, Integer gatewayId);

    @Transactional
    @Query("SELECT COUNT(d) FROM Device AS d WHERE d.gateway=:gatewayId")
    public Integer getGatewayDevicesCount(@Param("gatewayId") Gateway gatewayId);
}
