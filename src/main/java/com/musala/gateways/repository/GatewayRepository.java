package com.musala.gateways.repository;

import com.musala.gateways.model.Gateway;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayRepository extends CrudRepository<Gateway, Integer> {

    public Gateway findByUniqueSerialNumber(String uniqueSerialNumber);

}
