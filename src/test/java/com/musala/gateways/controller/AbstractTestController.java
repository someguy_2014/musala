package com.musala.gateways.controller;

import com.musala.gateways.service.DeviceService;
import com.musala.gateways.service.GatewayService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractTestController {

    protected static final String BASE_GATEWAY_URL = "/gateway/";
    protected static final String BASE_DEVICE_URL = "/device/";

    @Autowired
    protected TestRestTemplate restTemplate;

    @MockBean
    protected DeviceService deviceService;

    @MockBean
    protected GatewayService gatewayService;

}
