package com.musala.gateways.controller;

import static com.musala.database.FactoryBot.createGateway;
import static com.musala.database.FactoryBot.createGatewayWithInvalidData;
import static com.musala.gateways.controller.AbstractTestController.BASE_GATEWAY_URL;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.dto.GatewayDto;
import static com.musala.gateways.service.GatewayService.SERIAL_NUMBER_ALREADY_EXISTS;
import java.util.Arrays;
import java.util.List;
import javax.validation.ValidationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

public class GatewayControllerTest extends AbstractTestController {

    @Test
    public void testGetGateways() throws Exception {
        Gateway gateway = createGateway(1);
        when(gatewayService.getGateways()).thenReturn(Arrays.asList(gateway));

        ResponseEntity<List<GatewayDto>> gatewayResponse = getForGatewaysEntity();

        List<GatewayDto> gatewayDtoResult = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.OK);
        assertNotNull(gatewayDtoResult);
        assertEquals(gatewayDtoResult.size(), 1);
        assertEquals(gatewayDtoResult.get(0).getId(), gateway.getId());
        verify(gatewayService).getGateways();
    }

    @Test
    public void testGetGateway() throws Exception {
        Gateway gateway = createGateway(1);
        when(gatewayService.getGateway(gateway.getId())).thenReturn(gateway);

        ResponseEntity<GatewayDto> gatewayResponse = getForGatewayEntity(gateway.getId());

        GatewayDto gatewayDtoResult = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.OK);
        assertNotNull(gatewayDtoResult);
        assertEquals(gatewayDtoResult.getId(), gateway.getId());
        assertEquals(gatewayDtoResult.getIp(), gateway.getIp());
        assertEquals(gatewayDtoResult.getUniqueSerialNumber(), gateway.getUniqueSerialNumber());
        assertEquals(gatewayDtoResult.getName(), gateway.getName());
        verify(gatewayService).getGateway(gateway.getId());
    }

    @Test
    public void testGetGatewayNotFound() throws Exception {
        Integer gatewayId = 1;
        String errorMessage = "Could not find resource with id: " + gatewayId;
        when(gatewayService.getGateway(gatewayId)).thenThrow(new ResourceNotFoundException(errorMessage));

        ResponseEntity<List> gatewayResponse = getForInvalidGatewayEntity(gatewayId);

        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.NOT_FOUND);
        assertEquals(gatewayResponse.getBody(), Arrays.asList(errorMessage));
        verify(gatewayService).getGateway(gatewayId);
    }
    
    @Test
    public void testAddGateway() throws Exception {
        Gateway gateway = createGateway(1);
        doNothing().when(gatewayService).validateUniqueSerialNumber(gateway);
        when(gatewayService.addGateway(any())).thenReturn(gateway);

        ResponseEntity<GatewayDto> gatewayResponse = postForGatewayEntity();

        GatewayDto gatewayDtoResult = gatewayResponse.getBody();
        assertNotNull(gatewayDtoResult);
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.CREATED);
        assertEquals(gatewayDtoResult.getId(), gateway.getId());
        assertEquals(gatewayDtoResult.getIp(), gateway.getIp());
        assertEquals(gatewayDtoResult.getUniqueSerialNumber(), gateway.getUniqueSerialNumber());
        assertEquals(gatewayDtoResult.getName(), gateway.getName());
        verify(gatewayService).addGateway(any());
    }

    @Test
    public void testAddGatewayWithInvalidIp() throws Exception {
        Gateway gateway = createGatewayWithInvalidData();
        
        ResponseEntity<List> gatewayResponse = postForInvalidGatewayEntity(gateway);

        List errors = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(errors.contains("Enter valid IPv4 IP"));
        verify(gatewayService, never()).addGateway(any());
    }
    
    @Test
    public void testAddGatewayWithInvalidUniqueSerialNumber() throws Exception {
        Gateway gateway = createGateway(null);
        doThrow(new ValidationException(SERIAL_NUMBER_ALREADY_EXISTS)).when(gatewayService).addGateway(any());
        
        ResponseEntity<List> gatewayResponse = postForInvalidGatewayEntity(gateway);

        List errors = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertTrue(errors.contains(SERIAL_NUMBER_ALREADY_EXISTS));
        verify(gatewayService).addGateway(any());
    }

    @Test
    public void testUpdateGateway() throws Exception {
        Gateway gateway = createGateway(1);
        doNothing().when(gatewayService).validateUniqueSerialNumber(gateway);
        when(gatewayService.editGateway(eq(gateway.getId()), any(Gateway.class))).thenReturn(gateway);

        ResponseEntity<GatewayDto> gatewayResponse = putForGatewayEntity(gateway);

        GatewayDto gatewayDtoResult = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.OK);
        assertNotNull(gatewayDtoResult);
        assertEquals(gatewayDtoResult.getId(), gateway.getId());
        assertEquals(gatewayDtoResult.getIp(), gateway.getIp());
        assertEquals(gatewayDtoResult.getUniqueSerialNumber(), gateway.getUniqueSerialNumber());
        assertEquals(gatewayDtoResult.getName(), gateway.getName());
        verify(gatewayService).editGateway(eq(gateway.getId()), any(Gateway.class));
    }
    
    @Test
    public void deleteUpdateGateway() throws Exception {
        Gateway gateway = createGateway(1);
        when(gatewayService.deleteGateway(eq(gateway.getId()))).thenReturn(gateway);

        ResponseEntity<GatewayDto> gatewayResponse = deleteForGatewayEntity(gateway);

        GatewayDto gatewayDtoResult = gatewayResponse.getBody();
        assertEquals(gatewayResponse.getStatusCode(), HttpStatus.OK);
        assertNotNull(gatewayDtoResult);
        assertEquals(gatewayDtoResult.getId(), gateway.getId());
        assertEquals(gatewayDtoResult.getIp(), gateway.getIp());
        assertEquals(gatewayDtoResult.getUniqueSerialNumber(), gateway.getUniqueSerialNumber());
        assertEquals(gatewayDtoResult.getName(), gateway.getName());
        verify(gatewayService).deleteGateway(eq(gateway.getId()));
    }
    
    protected ResponseEntity<GatewayDto> postForGatewayEntity() {
        HttpEntity<GatewayDto> request = new HttpEntity<>(GatewayDto.from(createGateway(null)));
        return restTemplate.postForEntity(BASE_GATEWAY_URL, request, GatewayDto.class);
    }

    protected ResponseEntity<List> postForInvalidGatewayEntity(Gateway gateway) {
        HttpEntity<GatewayDto> request = new HttpEntity<>(GatewayDto.from(gateway));
        return restTemplate.postForEntity(BASE_GATEWAY_URL, request, List.class);
    }

    protected ResponseEntity<GatewayDto> getForGatewayEntity(Integer gatewayId) {
        return restTemplate.getForEntity(BASE_GATEWAY_URL  + gatewayId, GatewayDto.class);
    }

    protected ResponseEntity<List> getForInvalidGatewayEntity(Integer gatewayId) {
        return restTemplate.getForEntity(BASE_GATEWAY_URL  + gatewayId, List.class);
    }

    protected ResponseEntity<List<GatewayDto>> getForGatewaysEntity() {
        ParameterizedTypeReference<List<GatewayDto>> responseType = new ParameterizedTypeReference<List<GatewayDto>>() {
        };
        return restTemplate.exchange(BASE_GATEWAY_URL, HttpMethod.GET, null, responseType);
    }
    
    protected ResponseEntity<GatewayDto> putForGatewayEntity(Gateway gateway) {
        HttpEntity<GatewayDto> request = new HttpEntity<>(GatewayDto.from(gateway));
        return restTemplate.exchange(BASE_GATEWAY_URL  + gateway.getId(), HttpMethod.PUT, request, GatewayDto.class);
    }
    
    protected ResponseEntity<GatewayDto> deleteForGatewayEntity(Gateway gateway) {
        return restTemplate.exchange(BASE_GATEWAY_URL  + gateway.getId(), HttpMethod.DELETE, null, GatewayDto.class);
    }
}
