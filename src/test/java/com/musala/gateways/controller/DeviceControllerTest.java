package com.musala.gateways.controller;

import static com.musala.database.FactoryBot.createGateway;
import static com.musala.database.FactoryBot.createGatewayDevice;
import static com.musala.gateways.controller.AbstractTestController.BASE_DEVICE_URL;
import static com.musala.gateways.controller.AbstractTestController.BASE_GATEWAY_URL;
import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.dto.DeviceDto;
import com.musala.gateways.service.DeviceService;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import javax.validation.ValidationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

public class DeviceControllerTest extends AbstractTestController {

    private static final String DEVICES_MAX_SIZE_ERROR_MESSAGE = MessageFormat.format(DeviceService.DEVICES_MAX_SIZE_ERROR_MESSAGE_TEMPLATE, 10);

    @Test
    public void testGetDevices() throws Exception {
        Gateway gateway = createGateway(1);
        Device device = createGatewayDevice(1);
        when(deviceService.getDevices(gateway.getId())).thenReturn(Arrays.asList(device));

        ResponseEntity<List<DeviceDto>> deviceResponse = getForDevicesEntity(gateway.getId());

        List<DeviceDto> deviceDtoResult = deviceResponse.getBody();
        assertEquals(deviceResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(deviceDtoResult.size(), 1);
        assertEquals(deviceDtoResult.get(0).getId(), device.getId());
        verify(deviceService).getDevices(gateway.getId());
    }

    @Test
    public void testGetDevice() throws Exception {
        Integer gatewayId = 1;
        Device device = createGatewayDevice(1);
        when(deviceService.getDevice(gatewayId, device.getId())).thenReturn(device);

        ResponseEntity<DeviceDto> deviceResponse = getForDeviceEntity(gatewayId, device.getId());

        DeviceDto deviceDtoResult = deviceResponse.getBody();
        assertEquals(deviceResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(deviceDtoResult.getId(), device.getId());
        assertEquals(deviceDtoResult.getVendor(), device.getVendor());
        assertEquals(deviceDtoResult.getStatus(), device.getStatus());
        verify(deviceService).getDevice(gatewayId, device.getId());
    }

    @Test
    public void testGetDeviceNotFound() throws Exception {
        Integer gatewayId = 1;
        Device device = createGatewayDevice(1);
        String errorMessage = MessageFormat.format(DeviceService.RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, device.getId(), gatewayId);
        when(deviceService.getDevice(gatewayId, device.getId())).thenThrow(new ResourceNotFoundException(errorMessage));

        ResponseEntity<List> deviceResponse = getForInvalidDeviceEntity(gatewayId, device.getId());

        assertEquals(deviceResponse.getStatusCode(), HttpStatus.NOT_FOUND);
        assertEquals(deviceResponse.getBody(), Arrays.asList(errorMessage));
        verify(deviceService).getDevice(gatewayId, device.getId());
    }

    @Test
    public void testAddDevice() throws Exception {
        Gateway gateway = createGateway(1);
        Device device = createGatewayDevice(1);
        when(gatewayService.getGateway(gateway.getId())).thenReturn(gateway);
        when(deviceService.addDevice(any(), any())).thenReturn(device);

        ResponseEntity<DeviceDto> deviceResponse = postForDeviceEntity(gateway.getId());

        DeviceDto deviceDtoResult = deviceResponse.getBody();
        assertEquals(deviceResponse.getStatusCode(), HttpStatus.CREATED);
        assertEquals(deviceDtoResult.getId(), device.getId());
        assertEquals(deviceDtoResult.getVendor(), device.getVendor());
        verify(gatewayService).getGateway(gateway.getId());
        verify(deviceService).addDevice(any(), any());
    }

    @Test
    public void testAddDeviceMaxSizeExceeded() throws Exception {
        Gateway gateway = createGateway(1);
        when(gatewayService.getGateway(gateway.getId())).thenReturn(gateway);
        when(deviceService.addDevice(any(), any())).thenThrow(new ValidationException(DEVICES_MAX_SIZE_ERROR_MESSAGE));

        ResponseEntity<List> deviceResponse = postForInvalidDeviceEntity(gateway.getId());

        assertEquals(deviceResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
        assertEquals(deviceResponse.getBody(), Arrays.asList(DEVICES_MAX_SIZE_ERROR_MESSAGE));
        verify(gatewayService).getGateway(gateway.getId());
        verify(deviceService).addDevice(any(), any());
    }

    @Test
    public void testUpdateDevice() throws Exception {
        Gateway gateway = createGateway(1);
        Device device = createGatewayDevice(1);
        when(deviceService.editDevice(eq(gateway.getId()), eq(device.getId()), any(Device.class))).thenReturn(device);

        ResponseEntity<DeviceDto> deviceResponse = putForGatewayEntity(gateway.getId(), device);

        DeviceDto deviceDtoResult = deviceResponse.getBody();
        assertNotNull(deviceDtoResult);
        assertEquals(deviceResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(deviceDtoResult.getId(), device.getId());
        assertEquals(deviceDtoResult.getVendor(), device.getVendor());
        verify(deviceService).editDevice(eq(gateway.getId()), eq(device.getId()), any(Device.class));
    }
    
    @Test
    public void testDeleteDevice() throws Exception {
        Gateway gateway = createGateway(1);
        Device device = createGatewayDevice(1);
        when(deviceService.deleteDevice(eq(gateway.getId()), eq(device.getId()))).thenReturn(device);

        ResponseEntity<DeviceDto> deviceResponse = deleteForGatewayEntity(gateway.getId(), device);

        DeviceDto deviceDtoResult = deviceResponse.getBody();
        assertNotNull(deviceDtoResult);
        assertEquals(deviceResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(deviceDtoResult.getId(), device.getId());
        assertEquals(deviceDtoResult.getVendor(), device.getVendor());
        verify(deviceService).deleteDevice(eq(gateway.getId()), eq(device.getId()));
    }

    protected ResponseEntity<DeviceDto> postForDeviceEntity(Integer gatewayId) {
        HttpEntity<DeviceDto> request = new HttpEntity<>(DeviceDto.from(createGatewayDevice(null)));
        return restTemplate.postForEntity(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL, request, DeviceDto.class);
    }

    protected ResponseEntity<List> postForInvalidDeviceEntity(Integer gatewayId) {
        HttpEntity<DeviceDto> request = new HttpEntity<>(DeviceDto.from(createGatewayDevice(null)));
        return restTemplate.postForEntity(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL, request, List.class);
    }

    protected ResponseEntity<DeviceDto> getForDeviceEntity(Integer gatewayId, Integer deviceId) {
        return restTemplate.getForEntity(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL  + deviceId, DeviceDto.class);
    }

    protected ResponseEntity<List> getForInvalidDeviceEntity(Integer gatewayId, Integer deviceId) {
        return restTemplate.getForEntity(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL  + deviceId, List.class);
    }

    protected ResponseEntity<List<DeviceDto>> getForDevicesEntity(Integer gatewayId) {
        ParameterizedTypeReference<List<DeviceDto>> responseType = new ParameterizedTypeReference<List<DeviceDto>>() {
        };
        return restTemplate.exchange(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL, HttpMethod.GET, null, responseType);
    }

    protected ResponseEntity<DeviceDto> putForGatewayEntity(Integer gatewayId, Device device) {
        HttpEntity<DeviceDto> request = new HttpEntity<>(DeviceDto.from(device));
        return restTemplate.exchange(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL  + device.getId(), HttpMethod.PUT, request, DeviceDto.class);
    }
    
    protected ResponseEntity<DeviceDto> deleteForGatewayEntity(Integer gatewayId, Device device) {
        return restTemplate.exchange(BASE_GATEWAY_URL  + gatewayId + BASE_DEVICE_URL  + device.getId(), HttpMethod.DELETE, null, DeviceDto.class);
    }
}
