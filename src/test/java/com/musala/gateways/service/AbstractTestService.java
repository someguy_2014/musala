package com.musala.gateways.service;

import com.musala.config.TestConfig;
import com.musala.config.TestContextLoader;
import com.musala.database.DatabaseCleaner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(loader = TestContextLoader.class, classes = TestConfig.class)
@SpringBootTest
public class AbstractTestService {

    @Autowired
    protected DeviceService deviceService;

    @Autowired
    protected GatewayService gatewayService;
    
    @Autowired 
    DatabaseCleaner databaseCleaner;

    @AfterEach
    public void afterEach() {
       databaseCleaner.truncate();
    }

}
