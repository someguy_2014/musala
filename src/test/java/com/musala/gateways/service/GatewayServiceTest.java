package com.musala.gateways.service;

import static com.musala.database.FactoryBot.createGateway;
import com.musala.gateways.model.Gateway;
import static com.musala.gateways.service.GatewayService.RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE;
import static com.musala.gateways.service.GatewayService.SERIAL_NUMBER_ALREADY_EXISTS;
import java.text.MessageFormat;
import java.util.List;
import javax.validation.ValidationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

public class GatewayServiceTest extends AbstractTestService {

    @Test
    public void testAddGateway() {
        Gateway gateway = createGateway(null);

        Gateway result = gatewayService.addGateway(gateway);

        assertNotNull(result.getId());
        assertEquals(result.getId(), gateway.getId());
        assertEquals(result.getName(), gateway.getName());
        assertEquals(result.getUniqueSerialNumber(), gateway.getUniqueSerialNumber());
        assertEquals(result.getIp(), gateway.getIp());
        assertNotNull(result.getDateCreated());
        assertNotNull(result.getDateUpdated());
    }

    @Test
    public void testGetGateways() {
        Integer countBefore = gatewayService.getGateways().size();
        gatewayService.addGateway(createGateway(null));

        List<Gateway> gateways = gatewayService.getGateways();

        assertTrue(gateways.size() > countBefore);
    }

    @Test
    public void testGetGateway() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));

        Gateway result = gatewayService.getGateway(gateway.getId());

        assertEquals(result.getId(), gateway.getId());
    }

    @Test
    public void testGatewayNotFound() {
        Integer gatewayId = 1;
        String notFoundExceptionErrorMessage = MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, gatewayId);

        ResourceNotFoundException notFoundException = assertThrows(ResourceNotFoundException.class, () -> gatewayService.getGateway(gatewayId));

        assertEquals(notFoundException.getMessage(), notFoundExceptionErrorMessage);
    }

    @Test
    public void testDeleteGateway() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));

        Gateway deleted = gatewayService.deleteGateway(gateway.getId());

        assertEquals(deleted.getId(), gateway.getId());
    }

    @Test
    public void testEditGateway() {
        Gateway oldGateway = gatewayService.addGateway(createGateway(null));
        Gateway gatewayForEdit = gatewayService.getGateway(oldGateway.getId());
        gatewayForEdit.setIp("192.168.1.1");
        gatewayForEdit.setName("updated name");
        gatewayForEdit.setUniqueSerialNumber("new serial number");

        Gateway updatedGateway = gatewayService.editGateway(gatewayForEdit.getId(), gatewayForEdit);

        assertEquals(updatedGateway.getId(), gatewayForEdit.getId());
        assertEquals(updatedGateway.getName(), gatewayForEdit.getName());
        assertEquals(updatedGateway.getUniqueSerialNumber(), gatewayForEdit.getUniqueSerialNumber());
        assertEquals(updatedGateway.getIp(), gatewayForEdit.getIp());
        assertEquals(updatedGateway.getDateCreated(), gatewayForEdit.getDateCreated());
        assertNotEquals(updatedGateway.getDateUpdated(), gatewayForEdit.getDateUpdated());
    }

    @Test
    public void testValidTrue() {
        Gateway gateway = createGateway(null);

        gatewayService.validateUniqueSerialNumber(gateway);

    }

    @Test
    public void testValidFalse() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Gateway second = createGateway(null);

        gatewayService.validateUniqueSerialNumber(gateway);
        ValidationException validationException = assertThrows(ValidationException.class, () -> gatewayService.validateUniqueSerialNumber(second));

        assertEquals(validationException.getMessage(), SERIAL_NUMBER_ALREADY_EXISTS);
    }

}
