package com.musala.gateways.service;

import static com.musala.database.FactoryBot.createGateway;
import static com.musala.database.FactoryBot.createGatewayDevice;
import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.enums.DeviceStatus;
import static com.musala.gateways.service.DeviceService.DEVICES_MAX_SIZE_ERROR_MESSAGE_TEMPLATE;
import static com.musala.gateways.service.DeviceService.GATEWAY_DEVICES_MAX_SIZE;
import static com.musala.gateways.service.DeviceService.RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.IntStream;
import javax.validation.ValidationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

public class DeviceServiceTest extends AbstractTestService {

    @Test
    public void testAddDevice() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Device device = createGatewayDevice(null);

        Device result = deviceService.addDevice(gateway, device);

        assertNotNull(result.getId());
        assertEquals(result.getId(), device.getId());
        assertEquals(result.getVendor(), device.getVendor());
        assertEquals(result.getStatus(), device.getStatus());
        assertEquals(result.getGateway().getId(), gateway.getId());
        assertNotNull(result.getDateCreated());
        assertNotNull(result.getDateUpdated());
    }

    @Test
    public void testAddOnlyTenDevicesToGateway() {
        int maxDevicesSize = GATEWAY_DEVICES_MAX_SIZE;
        Gateway gateway = gatewayService.addGateway(createGateway(null));

        IntStream.range(0, maxDevicesSize).forEach(i -> deviceService.addDevice(gateway, createGatewayDevice(null)));

        assertEquals(deviceService.getGatewayDevicesCount(gateway), maxDevicesSize);
    }

    @Test
    public void testAddGateweyDevicesUpToTheMaxLimit() {
        int maxDevicesSizeExceeded = GATEWAY_DEVICES_MAX_SIZE + 1;
        Gateway gateway = gatewayService.addGateway(createGateway(null));

        String devicesMaxSizeErrorMessage = MessageFormat.format(DEVICES_MAX_SIZE_ERROR_MESSAGE_TEMPLATE, GATEWAY_DEVICES_MAX_SIZE);

        ValidationException devicesMaxSizeException = assertThrows(ValidationException.class, () -> {
            addDevicesToGateway(gateway, maxDevicesSizeExceeded);
        });

        assertEquals(devicesMaxSizeException.getMessage(), devicesMaxSizeErrorMessage);
        assertEquals(deviceService.getGatewayDevicesCount(gateway), GATEWAY_DEVICES_MAX_SIZE);
    }

    @Test
    public void testGetDevices() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Integer countBefore = deviceService.getDevices(gateway.getId()).size();
        deviceService.addDevice(gateway, createGatewayDevice(null));

        List<Device> devices = deviceService.getDevices(gateway.getId());

        assertTrue(devices.size() > countBefore);
    }

    @Test
    public void testGetDevice() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Device device = deviceService.addDevice(gateway, createGatewayDevice(null));

        Device result = deviceService.getDevice(gateway.getId(), device.getId());

        assertEquals(result.getId(), device.getId());
        assertEquals(result.getGateway().getId(), gateway.getId());
    }

    @Test
    public void testDeviceNotFound() {
        Integer gatewayId = 1, deviceId = 1;
        String notFoundExceptionErrorMessage = MessageFormat.format(RESOURCE_NOT_FOUND_ERROR_MESSAGE_TEMPLATE, deviceId, gatewayId);

        ResourceNotFoundException notFoundException = assertThrows(ResourceNotFoundException.class, () -> deviceService.getDevice(gatewayId, deviceId));

        assertEquals(notFoundException.getMessage(), notFoundExceptionErrorMessage);
    }

    @Test
    public void testDeleteDevice() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Device device = deviceService.addDevice(gateway, createGatewayDevice(null));

        Device deleted = deviceService.deleteDevice(gateway.getId(), device.getId());

        assertEquals(deleted.getId(), device.getId());
    }

    @Test
    public void testEditDevice() {
        Gateway gateway = gatewayService.addGateway(createGateway(null));
        Device oldDevice = deviceService.addDevice(gateway, createGatewayDevice(null));
        Device deviceForEdit = deviceService.getDevice(gateway.getId(), oldDevice.getId());
        deviceForEdit.setStatus(DeviceStatus.offline);
        deviceForEdit.setVendor("updated vendor");

        Device updatedDevice = deviceService.editDevice(gateway.getId(), deviceForEdit.getId(), deviceForEdit);

        assertEquals(updatedDevice.getId(), deviceForEdit.getId());
        assertEquals(updatedDevice.getStatus(), deviceForEdit.getStatus());
        assertEquals(updatedDevice.getVendor(), deviceForEdit.getVendor());
        assertEquals(updatedDevice.getDateCreated(), deviceForEdit.getDateCreated());
        assertNotEquals(updatedDevice.getDateUpdated(), deviceForEdit.getDateUpdated());
    }

    private void addDevicesToGateway(Gateway gateway, Integer maxDevicesSize) {
        for (int i = 0; i < maxDevicesSize; i++) {
            deviceService.addDevice(gateway, createGatewayDevice(null));
        }
    }
}
