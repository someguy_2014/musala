package com.musala.database;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.springframework.transaction.annotation.Transactional;

public class DatabaseCleaner {

    private static final String CLEAN_GOAL = "flyway:clean -Dflyway.configFiles=test.properties";
    private static final String MIGRATE_GOAL = "flyway:migrate -Dflyway.configFiles=test.properties";
    private static final InvocationRequest request = getInvocationRequest();
    private static final Invoker invoker = new DefaultInvoker();

    private final EntityManager entityManager;

    public DatabaseCleaner(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public static void clean() {
        execute(Arrays.asList(CLEAN_GOAL));
    }

    public static void migrate() {
        execute(Arrays.asList(MIGRATE_GOAL));
    }

    public static void execute(List<String> goals) {
        try {
            request.setGoals(goals);
            invoker.execute(request);
        } catch (MavenInvocationException e) {
            e.printStackTrace();
        }
    }

    public static InvocationRequest getInvocationRequest() {
        Properties p = new Properties();
        try {
            p.load(new FileInputStream(System.getProperty("user.dir") + "/config/test.properties"));
        } catch (Throwable ex) {
            Logger.getLogger(DatabaseCleaner.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        String a = System.getProperty("user.dir") + p.getProperty("flyway_pom_path");
        InvocationRequest invocationRequest = new DefaultInvocationRequest();
        invocationRequest.setBatchMode(true);
        invocationRequest.setPomFile(new File(System.getProperty("user.dir") + p.getProperty("flyway_pom_path")));
        return invocationRequest;
    }

    @Transactional
    public void truncate() {
        entityManager.createNativeQuery("truncate devices;").executeUpdate();
        entityManager.createNativeQuery("delete from gateways;").executeUpdate();
        entityManager.createNativeQuery("alter table gateways auto_increment = 1;").executeUpdate();
        entityManager.flush();
    }
}
