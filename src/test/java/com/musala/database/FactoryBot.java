package com.musala.database;

import com.musala.gateways.model.Device;
import com.musala.gateways.model.Gateway;
import com.musala.gateways.model.enums.DeviceStatus;

public class FactoryBot {

    public static Gateway createGateway(Integer id) {
        Gateway gateway = new Gateway();

        gateway.setId(id);
        gateway.setName("Gateway 1");
        gateway.setUniqueSerialNumber("42a02e7e-2ac5-4da8-8edf-a37a363fc0fd");
        gateway.setIp("192.168.10.111");

        return gateway;
    }

    public static Gateway createGatewayWithInvalidData() {
        Gateway gateway = createGateway(null);

        gateway.setIp("invalid ip");

        return gateway;
    }

    public static Device createGatewayDevice(Integer id) {
        Device device = new Device();

        device.setId(id);
        device.setStatus(DeviceStatus.online);
        device.setVendor("Device vendor");
        device.setId(id);

        return device;
    }

    
}
