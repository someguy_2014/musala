package com.musala.config;

import com.musala.database.DatabaseCleaner;
import javax.persistence.EntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class TestConfig {

    @Bean
    public DatabaseCleaner databaseCleaner(EntityManager entityManager) {
        return new DatabaseCleaner(entityManager);
    }

}
