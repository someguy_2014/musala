package com.musala.config;

import com.musala.database.DatabaseCleaner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootContextLoader;

public class TestContextLoader extends SpringBootContextLoader {

    @Override
    protected SpringApplication getSpringApplication() {
        SpringApplication app = super.getSpringApplication();

        super.getEnvironment().addActiveProfile("test");

        Runtime.getRuntime().addShutdownHook(new Thread(() -> DatabaseCleaner.clean()));

        DatabaseCleaner.clean();
        DatabaseCleaner.migrate();

        return app;
    }

}
